# pragma once

#include<cstdint>
#include<string>
#include<vector>

std::vector<uint8_t> rc4_encrypt_file(std::vector<uint8_t> plain_text, std::vector<uint8_t> key);