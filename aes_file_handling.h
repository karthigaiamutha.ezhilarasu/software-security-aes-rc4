# pragma once

#include<cstdint>
#include<string>
#include<vector>

std::vector<uint8_t> file_to_vector(std::string file_name);
std::vector<uint8_t> aes_encrypt_file(std::vector<uint8_t> plain_text, std::vector<uint8_t> key, int no_of_rounds);