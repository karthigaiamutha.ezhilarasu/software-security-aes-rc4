#include "avalanche_effect.h"
#include<iostream>
#include <bitset>

using namespace std;

float compute_avalanche_effect(vector<uint8_t> cipher_text1, vector<uint8_t> cipher_text2, int input_size){
    float variation = 0;
    float cipher_text_size = cipher_text1.size();

    for(int i=0;i<input_size;i++){
        if(cipher_text1[i] != cipher_text2[i]){
            bitset<8> cipher_bits1 = cipher_text1[i];
            bitset<8> cipher_bits2 = cipher_text2[i];
            for(int j=0;j<8;j++){
                if(cipher_bits1[j] != cipher_bits2[j]){
                    variation++;
                }
            }
        }
    }

    cout<<"No of changed bits = "<<variation<<", Total no of bits = "<<input_size*8<<'\n';
    
    float avalanche_effect = (variation/(input_size*8))*100;
    
    return avalanche_effect;
}