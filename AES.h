# pragma once

#include<vector>
#include<cstdint>

std::vector<uint8_t> aes_encrypt(std::vector<uint8_t> plain_text, std::vector<uint8_t> key, int no_of_rounds);
std::vector<uint8_t> aes_decrypt(std::vector<uint8_t> cipher_text, std::vector<uint8_t> key, int no_of_rounds);