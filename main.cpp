#include "rc4_file_handling.h"
#include "aes_file_handling.h"
#include "avalanche_effect.h"
#include<fstream>

#include<chrono>
#include<iostream>

using namespace std;
int main(){
    
    vector<uint8_t> key1 =  {'p','r','i','v','a','t','e','k','e','y','1','2','3','4','5','6'};
    vector<uint8_t> key2 =  {'p','r','i','v','a','t','e','k','d','y','1','2','3','4','5','6'};

    vector<uint8_t> data = file_to_vector("data.txt");

    int input_size = data.size();

    cout<<"\nInput size = "<<input_size*8<<" bits"<<'\n'<<'\n';

    auto start = chrono::high_resolution_clock::now();
    vector<uint8_t> aes_cipher_text1 = aes_encrypt_file(data, key1, 10);
    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    cout<<"Time taken for AES encryption (with key1) = "<<duration.count()/2<<" microseconds"<<'\n';

    start = chrono::high_resolution_clock::now();
    vector<uint8_t> aes_cipher_text2 = aes_encrypt_file(data, key2, 10);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    cout<<"Time taken for AES encryption (with Key2) = "<<duration.count()/2<<" microseconds"<<'\n'<<'\n';

    float avalanche_effect = compute_avalanche_effect(aes_cipher_text1,aes_cipher_text2,input_size);
    cout<<"avalanche effect of AES = "<<avalanche_effect<<"%"<<'\n'<<'\n';

    start = chrono::high_resolution_clock::now();
    vector<uint8_t> rc4_cipher_text1 = rc4_encrypt_file(data, key1);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    cout<<"Time taken for RC4 encryption (with Key1) = "<<duration.count()/2<<" microseconds"<<'\n';

    start = chrono::high_resolution_clock::now();
    vector<uint8_t> rc4_cipher_text2 = rc4_encrypt_file(data, key2);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    cout<<"Time taken for RC4 encryption (with Key2) = "<<duration.count()/2<<" microseconds"<<'\n'<<'\n';

    avalanche_effect = compute_avalanche_effect(rc4_cipher_text1,rc4_cipher_text2,input_size);
    cout<<"avalanche effect of RC4 = "<<avalanche_effect<<"%"<<'\n'<<'\n';

    start = chrono::high_resolution_clock::now();

    vector<uint8_t> aes_rc4_cipher_text1 = aes_encrypt_file(data, key1, 10);
    aes_rc4_cipher_text1 = rc4_encrypt_file(aes_rc4_cipher_text1, key1);

    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    cout<<"Time taken for AES-RC4 encryption (with Key1) = "<<duration.count()/2<<" microseconds"<<'\n';
    
    start = chrono::high_resolution_clock::now();
    
    vector<uint8_t> aes_rc4_cipher_text2 = aes_encrypt_file(data, key2, 10);
    aes_rc4_cipher_text2 = rc4_encrypt_file(aes_rc4_cipher_text2, key2);
    
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    cout<<"Time taken for AES-RC4 encryption (with Key2) = "<<duration.count()/2<<" microseconds"<<'\n'<<'\n';

    avalanche_effect = compute_avalanche_effect(aes_rc4_cipher_text1,aes_rc4_cipher_text2,input_size);
    cout<<"avalanche effect of AES-RC4 = "<<avalanche_effect<<"%"<<'\n'<<'\n';

    
    start = chrono::high_resolution_clock::now();
    vector<uint8_t> aes_rc4_reduced_rounds_cipher_text1 = aes_encrypt_file(data, key1, 6);
    aes_rc4_reduced_rounds_cipher_text1 = rc4_encrypt_file(aes_rc4_reduced_rounds_cipher_text1, key1);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    cout<<"Time taken for AES-RC4 encryption reduced rounds(with Key1) = "<<duration.count()/2<<" microseconds"<<'\n';
    
    start = chrono::high_resolution_clock::now();
    vector<uint8_t> aes_rc4_reduced_rounds_cipher_text2 = aes_encrypt_file(data, key2, 6);
    aes_rc4_reduced_rounds_cipher_text2 = rc4_encrypt_file(aes_rc4_reduced_rounds_cipher_text2, key2);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    cout<<"Time taken for AES-RC4 encryption reduced rounds(with Key2) = "<<duration.count()/2<<" microseconds"<<'\n'<<'\n';

    avalanche_effect = compute_avalanche_effect(aes_rc4_reduced_rounds_cipher_text1,aes_rc4_reduced_rounds_cipher_text2,input_size);
    cout<<"avalanche effect of AES-RC4 reduced rounds = "<<avalanche_effect<<"%"<<'\n'<<'\n';

    uint8_t ch1;
    uint8_t ch2;

    ifstream plain_text_file;
    ifstream decrypt_tet_file;

    plain_text_file.open("data.txt");
    decrypt_tet_file.open("decrypt.txt");
    
    float error_char = 0;
    float total_char = 0;

    while(plain_text_file){
        total_char++; 
        ch1 = plain_text_file.get();
        ch2 = decrypt_tet_file.get();
        if(ch1 != ch2){
            cout<<ch1<<ch2<<'\n';
            error_char++;
        }
    }
    float ber = (error_char*8) * 100 / (total_char*8);
    cout<<"Comparing 'data.txt' and 'decrypt.txt'"<<'\n';
    cout<<"BER = "<<ber<<'\n';

    plain_text_file.close();
    decrypt_tet_file.close();
}