#include<iostream>
#include<bit>
#include<bitset>
#include<cstdint>
#include<chrono>
#include "AES.h"

using namespace std;

static vector<vector<uint8_t>> expanded_key;

vector<vector<uint8_t>> array_to_block(vector<uint8_t> text);
void compute_expanded_key(vector<vector<uint8_t>> key_block);

vector<vector<uint8_t>> subBytes(vector<vector<uint8_t>> block);
vector<vector<uint8_t>> shiftRows(vector<vector<uint8_t>> block);
vector<vector<uint8_t>> mixColumns(vector<vector<uint8_t>> block);
vector<vector<uint8_t>> addRoundKey(vector<vector<uint8_t>> input_block,int round_number);

uint8_t rotl(uint8_t num, uint8_t shift_by);
vector<uint8_t> mixColumn(vector<uint8_t> column);
uint8_t mul3(uint8_t element);
uint8_t mul2(uint8_t element);

vector<vector<uint8_t>> get_round_key(vector<vector<uint8_t>> key_block, int round_number);
vector<uint8_t> compute_key_expansion(vector<uint8_t> word, int round_number);
vector<uint8_t> shiftKeyRows(vector<uint8_t> word);
vector<uint8_t> subituteKeyRows(vector<uint8_t> word);
vector<uint8_t> addRCon(vector<uint8_t> word, int round_number);
vector<uint8_t> exorWords(vector<uint8_t> word1, vector<uint8_t> word2);
vector<uint8_t> block_to_text(vector<vector<uint8_t>> block);

//decrypt
vector<vector<uint8_t>> d_shiftRows(vector<vector<uint8_t>> block);
vector<vector<uint8_t>> d_subBytes(vector<vector<uint8_t>> block);

vector<uint8_t> aes_encrypt(vector<uint8_t> plain_text, vector<uint8_t> key, int number_of_rounds){
    expanded_key.clear();
    
    vector<vector<uint8_t>> input_block = array_to_block(plain_text);
    vector<vector<uint8_t>> key_block = array_to_block(key);
    vector<vector<uint8_t>> cipher_block(4,vector<uint8_t>(4,0));
    
    auto start = chrono::high_resolution_clock::now();
   
    compute_expanded_key(key_block);
   
    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(stop - start);
    //cout<<"key_computation_time = "<<duration.count()<<" microseconds"<<'\n';

    start = chrono::high_resolution_clock::now();
    
    cipher_block = addRoundKey(input_block,0);
    vector<vector<uint8_t>> temp = cipher_block;

    //------------First 9 rounds-----------------
    for(int i=0;i<number_of_rounds-1;i++){
        cipher_block = subBytes(cipher_block);
        cipher_block = shiftRows(cipher_block);
        cipher_block = mixColumns(cipher_block);
        cipher_block = addRoundKey(cipher_block,i+1);
    }

    // //-------------Last Round---------------------

    cipher_block = subBytes(cipher_block);
    cipher_block = shiftRows(cipher_block);
    cipher_block = addRoundKey(cipher_block, number_of_rounds);

    vector<uint8_t> cipher_text = block_to_text(cipher_block);

    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::microseconds>(stop - start);

    return cipher_text;
}
vector<uint8_t> aes_decrypt(vector<uint8_t> cipher_text, vector<uint8_t> key, int number_of_rounds){
    expanded_key.clear();
    vector<vector<uint8_t>> key_block = array_to_block(key);
    compute_expanded_key(key_block);
    auto start = chrono::high_resolution_clock::now();

    vector<vector<uint8_t>> d_cipher_block = array_to_block(cipher_text);
    d_cipher_block = addRoundKey(d_cipher_block, number_of_rounds);
    d_cipher_block = d_shiftRows(d_cipher_block);
    d_cipher_block = d_subBytes(d_cipher_block);

    for(int i=number_of_rounds-1;i>0;i--){
        d_cipher_block = addRoundKey(d_cipher_block,i);
        d_cipher_block = mixColumns(d_cipher_block);
        d_cipher_block = mixColumns(d_cipher_block);
        d_cipher_block = mixColumns(d_cipher_block);
        d_cipher_block = d_shiftRows(d_cipher_block);
        d_cipher_block = d_subBytes(d_cipher_block);  
    }
    d_cipher_block = addRoundKey(d_cipher_block,0);

    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(stop - start);

    vector<uint8_t> decrypted_text = block_to_text(d_cipher_block);

    return decrypted_text;
}

vector<vector<uint8_t>> array_to_block(vector<uint8_t> text){
    vector<vector<uint8_t>> block(4,vector<uint8_t>(4,0));
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            block[j][i] = text[(i*4)+j];
        }
    }
    return block;
}

uint8_t rotl(uint8_t num, uint8_t shift_by){
    uint16_t shifted_num = num << shift_by;
    uint16_t upper_nibble = (shifted_num & 0xFF00) >> 8;
    uint16_t lower_nibble = (shifted_num & 0x00FF);
    uint8_t rotated_num = (uint8_t)(upper_nibble|lower_nibble);
    return rotated_num;
}

vector<vector<uint8_t>> subBytes(vector<vector<uint8_t>> block){
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            uint8_t b = block[i][j];
            block[i][j] = b ^ (rotl(b,1)) ^ (rotl(b,2)) ^ (rotl(b,3)) ^ (rotl(b,4)) ^ 0x63;
        }
    }
    return block;
}

vector<vector<uint8_t>> shiftRows(vector<vector<uint8_t>> block){
    vector<vector<uint8_t>> temp = block;
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            block[i][j] = temp[i][(j+i)%4];
        }
    }
    return block;
}

vector<vector<uint8_t>> mixColumns(vector<vector<uint8_t>> block){
    for(int i=0;i<4;i++){
        vector<uint8_t> mixed_column = mixColumn({block[0][i],block[1][i],block[2][i],block[3][i]});
        for(int j=0;j<4;j++){
            block[j][i] = mixed_column[j];
        }
    }
    return block;
}

vector<uint8_t> mixColumn(vector<uint8_t> column){
    uint8_t mixed_column1 = (mul2(column[0]) ^ mul3(column[1]) ^ column[2] ^ column[3]);
    uint8_t mixed_column2 = (column[0] ^ mul2(column[1]) ^ mul3(column[2]) ^ column[3]);
    uint8_t mixed_column3 = (column[0] ^ column[1] ^ mul2(column[2]) ^ mul3(column[3]));
    uint8_t mixed_column4 = (mul3(column[0]) ^ column[1] ^ column[2] ^ mul2(column[3]));
    return {mixed_column1, mixed_column2, mixed_column3, mixed_column4};
}

uint8_t mul3(uint8_t element){
    return (mul2(element) ^ element);
}

uint8_t mul2(uint8_t element){
    bool higher_bit = element & 0x80;
    uint8_t doubled = (element << 1) & 0xFF;
    if(higher_bit){
        doubled = doubled ^  0x1B;
    }
    return doubled;
}

void compute_expanded_key(vector<vector<uint8_t>> key_block){
    for(int i=0;i<4;i++){
        expanded_key.push_back({key_block[0][i],key_block[1][i],key_block[2][i],key_block[3][i]});
    }
    vector<vector<uint8_t>> previous_key = expanded_key;
    vector<vector<uint8_t>> current_key(4,vector<uint8_t>(4,0));
    vector<uint8_t> key_expansion;
    for(int i=0;i<10;i++){
        key_expansion = compute_key_expansion(previous_key[3], i+1);
        current_key[0] = exorWords(previous_key[0],key_expansion);
        expanded_key.push_back(current_key[0]);
        for(int j=1;j<4;j++){
            current_key[j] = exorWords(current_key[j-1],previous_key[j]);
            expanded_key.push_back(current_key[j]);
        }
        previous_key = current_key;
    }
}

vector<uint8_t> compute_key_expansion(vector<uint8_t> word, int round_number){
    word = shiftKeyRows(word);
    word = subituteKeyRows(word);
    word = addRCon(word, round_number);
    return word;
}

vector<uint8_t> shiftKeyRows(vector<uint8_t> word){
    vector<uint8_t> shifted_word(4,0);
    for(int i=0;i<4;i++){
        shifted_word[i] = word[(i+1)%4];
    }
    return shifted_word;
}

vector<uint8_t> subituteKeyRows(vector<uint8_t> word){
    for(int i=0;i<4;i++){
        word[i] = word[i] ^ (rotl(word[i],1)) ^ (rotl(word[i],2)) ^ (rotl(word[i],3)) ^ (rotl(word[i],4)) ^ 0x63;
    }
    return word;
}

vector<uint8_t> addRCon(vector<uint8_t> word, int round_number){
    vector<int> rcon = {0x01, 0x02,	0x04, 0x08,	0x10, 0x20, 0x40, 0x80, 0x1B, 0x36};
    word[0] = word[0] ^ rcon[round_number - 1];
    return word;
}

vector<uint8_t> exorWords(vector<uint8_t> word1, vector<uint8_t> word2){
    vector<uint8_t> exored(4,0);
    for(int i=0;i<4;i++){
        exored[i] = (word1[i] ^ word2[i]);
    }
    return exored;
}

vector<vector<uint8_t>> addRoundKey(vector<vector<uint8_t>> input, int round_number){
    vector<vector<uint8_t>> output(4, vector<uint8_t>(4,0));
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            output[i][j] = input[i][j] ^ expanded_key[round_number*i][j];
        }
    }
    return output;
}

vector<uint8_t> block_to_text(vector<vector<uint8_t>> block){
    vector<uint8_t> text(16,'0');
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            text[(i*4)+j] = block[j][i];
        }
    }
    return text;
}

vector<vector<uint8_t>> d_shiftRows(vector<vector<uint8_t>> block){
    vector<vector<uint8_t>> temp = block;

    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            block[i][(j+i)%4] = temp[i][j];
        }
    }
    return block;
}

vector<vector<uint8_t>> d_subBytes(vector<vector<uint8_t>> block){
    for(int i=0;i<4;i++){
     for(int j=0;j<4;j++){
         uint8_t b = block[i][j];
         block[i][j] = (rotl(b,1)) ^ (rotl(b,3)) ^ (rotl(b,6)) ^ 0x05;
      }
    }
    return block;
}