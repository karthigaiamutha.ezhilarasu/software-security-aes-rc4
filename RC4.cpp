#include "RC4.h"
#include<iostream>

using namespace std;

static int PRGA_j = 0;
static int PRGA_k = 0;

void key_scheduling_algorithm(vector<uint8_t>& S, vector<uint8_t> key);
uint8_t psuedo_random_genrator(vector<uint8_t> &S, int i);


vector<uint8_t> rc4_encrypt(vector<uint8_t> plain_text, vector<uint8_t> key){
    vector<uint8_t> S(256,0); //State Vector
    int plain_text_length = plain_text.size();
    PRGA_j = 0;
    PRGA_k = 0;

    uint8_t key_byte = 0;
    vector<uint8_t> cipher_text(plain_text_length,0);
    
    key_scheduling_algorithm(S,key);

    for(int i=0;i<plain_text.size();i++){
        key_byte = psuedo_random_genrator(S, i);
        cipher_text[i] = plain_text[i]^key_byte;
    }

    return cipher_text;
}

vector<uint8_t> rc4_decrypt(vector<uint8_t> cipher_text, vector<uint8_t> key){
    vector<uint8_t> S(256,0); //State Vector
    PRGA_j = 0;
    PRGA_k = 0;

    uint8_t key_byte = 0;
    vector<uint8_t> decrypted_text;

    key_scheduling_algorithm(S,key);

    for(int i=0;i<cipher_text.size();i++){
        key_byte = psuedo_random_genrator(S, i);
        decrypted_text.push_back(cipher_text[i]^key_byte);
    }
    return decrypted_text;
}

void key_scheduling_algorithm(vector<uint8_t>& S, vector<uint8_t> key){
    vector<uint8_t> T(256,0); //Temperory Vector
    int key_length = key.size();

    for(int i=0;i<256;i++){
        S[i] = i;
        T[i] = key[i % key_length];
    }

    int j = 0;

    for(int i=0;i<256;i++){
        j = (j+S[i]+T[i]) % 256;
        swap(S[i],S[j]);
    }
}

uint8_t psuedo_random_genrator(vector<uint8_t> &S, int i){
    PRGA_j = (PRGA_j + S[(i+1)%256]) % 256;
    swap(S[i],S[PRGA_j]);
    PRGA_k = (S[i] + S[PRGA_j])%256;
    return S[PRGA_k];
}