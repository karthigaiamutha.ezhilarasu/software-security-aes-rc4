main: main.o aes.o aes_file_handling.o rc4.o rc4_file_handling.o avalanche_effect.o
	g++ main.o aes.o aes_file_handling.o rc4.o rc4_file_handling.o avalanche_effect.o -o main

main.o: main.cpp
	g++ -c main.cpp

aes.o: AES.cpp
	g++ -c AES.cpp

aes_file_handling.o: aes_file_handling.cpp
	g++ -c aes_file_handling.cpp

rc4.o: RC4.cpp
	g++ -c RC4.cpp

rc4_file_handling.o: rc4_file_handling.cpp
	g++ -c rc4_file_handling.cpp

avalanche_effect.o: avalanche_effect.cpp
	g++ -c avalanche_effect.cpp

clean:
	-del *.o $(BUILD_DIR)