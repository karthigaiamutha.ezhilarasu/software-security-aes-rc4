#pragma once

#include<vector>
#include<cstdint>

std::vector<uint8_t> rc4_encrypt(std::vector<uint8_t> plain_text, std::vector<uint8_t> key);
std::vector<uint8_t> rc4_decrypt(std::vector<uint8_t> cipher_text, std::vector<uint8_t> key);
