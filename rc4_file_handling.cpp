#include<fstream>
#include<iostream>
#include "RC4.h"
#include "rc4_file_handling.h"

using namespace std;

vector<uint8_t> rc4_encrypt_file(vector<uint8_t> plain_text, vector<uint8_t> key){
 
    ofstream cipher_text_file;
    cipher_text_file.open("rc4_cipher.txt");

    ofstream decrypted_text_file;
    decrypted_text_file.open("rc4_decrypt.txt");

    vector<uint8_t> cipher_text;
    vector<uint8_t> decrypted_text;

    cipher_text = rc4_encrypt(plain_text,key);
            
    for(int i=0;i<cipher_text.size();i++){
        cipher_text_file<<cipher_text[i];
    }

    decrypted_text = rc4_decrypt(cipher_text,key);
       
    for(int i=0;i<cipher_text.size();i++){
        decrypted_text_file<<decrypted_text[i];
    }

    cipher_text_file.close();
    decrypted_text_file.close();
    return cipher_text;
}