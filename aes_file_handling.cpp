#include<fstream>
#include<iostream>
#include "AES.h"
#include "aes_file_handling.h"

using namespace std;

vector<uint8_t> file_to_vector(string file_name){
    uint8_t ch;
    ifstream plain_text_file;
    vector<uint8_t> plain_text;
    plain_text_file.open(file_name);
    while(plain_text_file){
        ch = plain_text_file.get();
        plain_text.push_back(ch);
    }
    return plain_text;
}

vector<uint8_t> aes_encrypt_file(vector<uint8_t> plain_text, vector<uint8_t> key, int no_of_rounds){

    vector<uint8_t> entire_cipher_text;

    ofstream cipher_text_file;
    cipher_text_file.open("cipher.txt");

    ofstream decrypted_text_file;
    decrypted_text_file.open("decrypt.txt");

    int m = 0;
    while(m<plain_text.size()){
        vector<uint8_t> plain_text_block(16,0);
        vector<uint8_t> cipher_text;
        vector<uint8_t> decrypted_text;

        for(int i=0;i<16;i++){
            plain_text_block[i] = plain_text[m];
            m++; 
        }

        cipher_text = aes_encrypt(plain_text_block,key, no_of_rounds);
            
        for(int i=0;i<16;i++){
            cipher_text_file<<cipher_text[i];
            entire_cipher_text.push_back(cipher_text[i]);
        }
        decrypted_text = aes_decrypt(cipher_text,key,no_of_rounds);
            
        for(int i=0;i<16;i++){
            decrypted_text_file<<decrypted_text[i];
        }
    }
    cipher_text_file.close();
    decrypted_text_file.close();
    return entire_cipher_text;
}