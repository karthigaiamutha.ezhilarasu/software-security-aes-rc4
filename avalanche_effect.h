#pragma once

#include<vector>
#include<cstdint>

float compute_avalanche_effect(std::vector<uint8_t> cipher1, std::vector<uint8_t> cipher2, int input_size);